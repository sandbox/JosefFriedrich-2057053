<?php

/**
 * @file
 * Page callbacks
 */

/**
 * Page callback: View calendars.
 *
 * @see pgn_calendar_menu()
 */
function pgn_calendar_view() {
  $tokens = array('pgn', 'teacher', 'q11', 'q12', 'holiday');

  $calendars = pgn_calendar_get_calendars_by_tokens($tokens);
  return pgn_calendar_get_view($calendars);
}

/**
 * Gets one view of a calendar.
 *
 * @param array $calendars
 *   The calendars to view.
 */
function pgn_calendar_get_view($calendars = array()) {

  $query = array();
  foreach ($calendars as $calendar) {
    $query[] = 'src=' . urlencode($calendar['id']);
    $query[] = 'color=' . urlencode($calendar['color']);
  }

  $src = array(
    'mode' => 'AGENDA',
    'height' => '800',
    'height' => '800',
    'wkst' => '2',
    'hl' => 'de',
    'bgcolor' => '#FFFFFF',
    'ctz' => 'Europe%2FBerlin',
  );

  foreach ($src as $key => $value) {
    $query[] = $key . '=' . urlencode($value);
  }

  $src = implode('&', $query);

  $attributes = array(
    'height' => '800',
    'frameborder' => '0',
    'scrolling' => 'no',
    'src' => 'https://www.google.com/calendar/embed?' . $src,
    'style' => 'border-width:0',
    'width' => '600',
  );

  $build['view_heading'] = array(
    '#markup' => '<hr><h3>Web-Ansicht</h3>',
  );

  $build['view_markup'] = array(
    '#markup' => '<iframe ' . drupal_attributes($attributes) . '></iframe>',
  );

  return $build;
}

/**
 * Page callback: View single calendar.
 *
 * @param string $token
 *   Calendar token.
 *
 * @return array
 *   Render array.
 *
 * @see pgn_calendar_menu()
 */
function pgn_calendar_view_calendar($token) {
  $calendar_ids = pgn_calendar_get_calendar_ids(array($token));
  $calendars = pgn_calendar_get_calendars($calendar_ids);

  $build['#calendars'] = $calendars;
  $build['subscribe'] = pgn_calendar_get_subscribe_table($calendars);
  $build['subscribe']['#weight'] = 1;
  $build['view'] = pgn_calendar_get_view($calendars);
  $build['view']['#weight'] = 2;

  drupal_alter('pgn_calendar_view_calendar', $build);

  return $build;
}

/**
 * Page callback: Subscribe calendars.
 *
 * @see pgn_calendar_menu()
 */
function pgn_calendar_subscribe() {
  $tokens = array('pgn', 'teacher', 'q11', 'q12', 'holiday');
  $calendars = pgn_calendar_get_calendars_by_tokens($tokens);

  return pgn_calendar_get_subscribe_table($calendars);
}

/**
 * Returns a HTML table with icons links to subscribe calendars.
 *
 * @param array $calendars
 *   The calendars.
 *
 * @return string
 *   HTML table output.
 */
function pgn_calendar_get_subscribe_table($calendars = array()) {
  $path = drupal_get_path('module', 'pgn_calendar') . '/images';

  $rows = array();

  $icons = array(
    'ical' => 'Importieren Sie den Kalender in ihr Smartphone im iCal-Format.',
    'xml' => 'Kalender-News-Feeds. Importieren Sie den RSS-Feed um über die neusten Termine informiert zu sein.',
    'html' => 'Zur Online-Ansicht des Kalenders',
  );
  foreach ($calendars as $calendar) {
    $row = array();
    $row[] = l($calendar['title_custom'], 'kalender/' . $calendar['token']);
    foreach ($icons as $icon => $description) {

      $variables = array(
        'path' => $path . '/' . $icon . '.png',
      );

      $options = array(
        'attributes' => array(
          'title' => $description,
        ),
        'html' => TRUE,
      );

      $row[] = l(theme('image', $variables), $calendar[$icon], $options);
    }
    $rows[] = $row;
  }

  $build['subscribe_heading'] = array(
    '#markup' => '<hr><h3>Kalender abonnieren</h3>',
  );

  $build['subscribe_table'] = array(
    '#theme' => 'table',
    '#header' => array('Kalender', 'iCal-Format', 'RSS-Feed', 'Online-Ansicht'),
    '#rows' => $rows,
  );

  return $build;
}

/**
 * Page callback: Flush pgn calendar cache.
 *
 * @see pgn_calendar_menu()
 */
function pgn_calendar_flush_cache() {
  cache_clear_all('pgn_calendar', 'cache', TRUE);
  drupal_set_message('Die Termine wurden erfolgreich mit dem Google Kalender synchronisiert.');
  drupal_goto();
}

/**
 * Page callback: Help page.
 *
 * @see pgn_calendar_menu()
 */
function pgn_calendar_help_overview() {
  $links = array();

  // PGN-Kalender in Thunderbird und Lightning importieren (über iCal-Format).
  $links[] = array(
    'title' => 'PGN-Kalender in Thunderbird und Lightning importieren (über iCal-Format)',
    'href' => url('node/10180', array('absolute' => TRUE)),
  );

  // PGN-Kalender in iCal auf den Mac (Lion) abonnieren (über CalDav).
  $links[] = array(
    'title' => 'PGN-Kalender in iCal auf den Mac (Lion) abonnieren (über CalDav)',
    'href' => url('node/10181', array('absolute' => TRUE)),
  );

  return theme('links', array('links' => $links));
}

/**
 * Admin form for the google api credentials.
 *
 * @see system_settings_form()
 */
function pgn_calendar_admin() {
  $form['pgn_calendar_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('pgn_calendar_user'),
  );

  $form['pgn_calendar_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('pgn_calendar_pass'),
  );

  return system_settings_form($form);
}
