<?php

/**
 * @file
 * Theme functions.
 */

/**
 * @addtogroup theme_functions
 * @{
 */

/**
 * Returns explanations for each calendars.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 *   - event
 */
function theme_pgn_pdfcalendar_explanation($variables) {
  $calendars = $variables['calendars'];
  $format = (array) $variables['format'];

  $format += array(
    'color' => TRUE,
  );

  foreach ($calendars as $calendar) {
    $cells = array();

    $width = 15;
    if (!$format['color']) {
      $calendar_token = '[' . $calendar['short'] . ']';
      $width = 25;
    }
    else {
      $calendar_token = $calendar['short'];
      $width = 15;
    }

    $cells[] = array(
      'data' => $calendar_token,
      'class' => 'calendar-short',
      'width' => $width,
    );
    if ($format['color']) {
      $cells[] = array(
        'data' => '',
        'class' => 'color',
        'width' => 8,
        'style' => 'background-color: ' . $calendar['color_custom'] . ';',
      );
    }
    $cells[] = array(
      'data' => $calendar['title_custom'],
      'class' => 'title-custom',
      'width' => 60,
    );
    $cells[] = array(
      'data' => isset($calendar['responsible']) ? $calendar['responsible'] : '',
      'class' => 'responsible',
      'width' => 200,
    );
    $rows[] = $cells;
  }

  // Header.
  $cells = array();
  $cells[] = array(
    'data' => '',
    'width' => $width,
  );
  if ($format['color']) {
    $cells[] = array(
      'data' => '',
      'width' => 8,
    );
  }
  $cells[] = array(
    'data' => '<strong>Kalendar</strong>',
    'width' => 60,
  );
  $cells[] = array(
    'data' => '<strong>Ansprechpartner</strong>',
    'width' => 200,
  );

  $table = array(
    'header' => $cells ,
    'rows' => $rows,
    'attributes' => array(
      'class' => array('explanation-table'),
      'cellpadding' => array(1),
      'cellspacing' => array(1),
    ),
  );

  return '<br/><p class="caption">Erläuterung:</p>' . theme('table', $table);

}

/**
 * Returns a formatted event text.
 *
 * @param array $variables
 *   An associative array containing:
 *   - event
 */
function theme_pgn_pdfcalendar_event_text($variables) {
  $event = $variables['event'];

  if (!isset($variables['format'])) {
    $variables['format'] = array();
  }

  $format = (array) $variables['format'];

  $format += array(
    'show_inline_date' => FALSE,
  );

  if (is_array($event)) {
    $output = '';
    if ($format['show_inline_date']) {

      $variables = array(
        'event' => $event,
        'format' => array(
          'short' => TRUE,
          'only_end' => TRUE,
        ),
      );
      $date = theme('pgn_calendar_date', $variables);

      if (!empty($date)) {
        $output .= '<strong>[' . $date . ']</strong> ';
      }
    }

    $output .= $event['title'];

    if (!isset($event['shift'])) {

      if (!empty($event['where'])) {
        $output .= ' <span class="where">[' . $event['where'] . ']</span>';
      }

      if (!empty($event['content'])) {
        $output .= ' <span class="content">(' . $event['content'] . ')</span>';
      }
    }
    return $output;
  }
}

/**
 * Returns a date string when the calendar was updated.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 *   - format
 */
function theme_pgn_pdfcalendar_updated($variables) {
  $calendars = $variables['calendars'];
  $format = (array) $variables['format'];

  $format += array(
    'short' => FALSE,
    'plain' => FALSE,
    'label' => TRUE,
  );

  $updated = array();
  foreach ($calendars as $calendar) {
    $updated[] = $calendar['updated'];
  }

  // The latest update time to top.
  rsort($updated);

  $date = new DateTime($updated[0]);
  $variables['updated'] = format_date($date->format('U'));

  if (!$format['short']) {
    $date = format_date($date->format('U'));
  }
  else {
    $date = format_date($date->format('U'), 'custom', 'd.m.Y');
  }

  if ($format['label']) {
    $label = 'Stand: ';
  }
  else {
    $label = '';
  }

  if (!$format['plain']) {
    $label = '<strong>' . $label . '</strong>';
  }

  $output = $label . $date;
  if (!$format['plain']) {
    $output = '<div class="updated">' . $output . '</div>';
  }

  return $output;
}

/**
 * Returns a formated period string.
 *
 * @param array $variables
 *   An associative array containing:
 *   - query
 *   - format
 */
function theme_pgn_pdfcalendar_period($variables) {
  $query = $variables['query'];
  $format = (array) $variables['format'];

  $format += array(
    'short' => FALSE,
    'plain' => FALSE,
    'label' => TRUE,
  );

  $start = pgn_calendar_date_format($query['start']);
  $end = pgn_calendar_date_format($query['end']);

  $output = '';
  if (!empty($query['semester']) && !empty($query['year'])) {

    $variables = array(
      'semester' => $query['semester'],
      'year' => $query['year'],
      'format' => array(
        'short' => $format['short'],
      ),
    );
    $prefix = theme('pgn_pdfcalendar_semester_title', $variables);
  }
  elseif (!empty($query['week'])) {
    if ($format['short']) {
      $prefix = 'Kalenderwoche ' . $query['week'];
    }
    else {
      $prefix = 'KW ' . $query['week'];
    }
    $prefix = 'Kalenderwoche ' . $query['week'];
  }

  if (!$format['short']) {
    $period = $start['day'] . '. ' . $start['month_word'] . ' ' . $start['year'];
    $period .= ' – ';
    $period .= $end['day'] . '. ' . $end['month_word'] . ' ' . $end['year'];
  }
  else {
    $period = $start['day'] . '.' . $start['month'] . '.' . substr($start['year'], 2);
    $period .= ' – ';
    $period .= $end['day'] . '.' . $end['month'] .  '.' . substr($end['year'], 2);
  }

  if (empty($prefix)) {
    $output = $period;
  }
  else {
    $output = $prefix . ' (' . $period . ')';
  }

  $format['label'] ? $label = 'Zeitraum: ' : $label = '';
  if (!$format['plain']) {
    $label = '<strong>' . $label . '</strong>';
  };

  $output = $label . $output;

  if (!$format['plain']) {
    $output = '<div class="period">' . $output . '</div>';
  };
  return $output;
}

/**
 * Returns a list of calendar abbreviations.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 */
function theme_pgn_pdfcalendar_calendar_short($variables) {
  $calendars = $variables['calendars'];

  $output = '';
  foreach ($calendars as $calendar) {
    $output .= $calendar['short'] . ', ';
  }

  return substr($output, 0, -2);
}

/**
 * Returns a list of responsible person for each calendar.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 */
function theme_pgn_pdfcalendar_responsible($variables) {
  $calendars = $variables['calendars'];

  $output = '';
  foreach ($calendars as $calendar) {
    if (!empty($calendar['responsible'])) {
      $output .= $calendar['title_custom'] . ': ';
      $output .= $calendar['responsible'] . ', ';
    }
  }

  return 'Verantwortlich: ' . substr($output, 0, -2);
}

/**
 * Returns a calendar summary.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 *   - prefix
 */
function theme_pgn_pdfcalendar_calendar_summary($variables) {
  $calendars = $variables['calendars'];
  $prefix = '<br/><p class="caption">Anmerkungen: </p><br/>';

  $output = '';
  foreach ($calendars as $calendar) {
    if (!empty($calendar['summary'])) {
      $output = '<p><strong>' . $calendar['title_custom'] . ': </strong>' . $calendar['summary'] . '</p><br/>';
    }
  }
  if (!empty($output)) {
    return '<div class="calendar-summary">' . $prefix . $output . '</div>';
  }
  else {
    return '';
  }
}

/**
 * Returns the filename for the rendered PDFs.
 *
 * @param array $variables
 *   An associative array containing:
 *   - calendars
 *   - query
 *   - prefix
 */
function theme_pgn_pdfcalendar_file_name($variables) {
  $calendars = $variables['calendars'];
  $query = $variables['query'];
  $prefix = $variables['prefix'];

  $output = '';
  empty($prefix) ? $output .= 'PGN-Terminplan' : $output .= $prefix;

  $variables = array(
    'calendars' => $calendars,
  );
  $output .= ' ' . theme('pgn_pdfcalendar_calendar_short', $variables);

  $variables = array(
    'query' => $query,
    'format' => array(
      'plain' => TRUE,
      'label' => FALSE,
      'short' => TRUE,
    ),
  );
  $output .= ' ' . theme('pgn_pdfcalendar_period', $variables);

  $variables = array(
    'calendars' => $calendars,
    'format' => array(
      'short' => TRUE,
      'plain' => TRUE,
    ),
  );
  $output .= ' (' . theme('pgn_pdfcalendar_updated', $variables) . ')';
  return $output;
}

/**
 * Returns the main calendar.
 *
 * @param array $variables
 *   An associative array containing:
 *   - events
 *   - format
 *
 * @ingroup main_calendar
 */
function theme_pgn_pdfcalendar_main($variables) {
  $events = $variables['events'];
  $format = (array) $variables['format'];

  $output = '';
  ksort($events);

  foreach ($events as $year => $months) {
    $variables = array(
      'months' => $months,
      'year' => $year,
      'format' => $format,
    );
    $output .= theme('pgn_pdfcalendar_main_month', $variables);
  }
  return $output;
}

/**
 * Returns the HTML output of one month.
 *
 * @param array $variables
 *   An associative array containing:
 *   - months
 *   - year
 *   - format
 *
 * @ingroup main_calendar
 */
function theme_pgn_pdfcalendar_main_month($variables) {
  $months = $variables['months'];
  $year = $variables['year'];
  $format = (array) $variables['format'];

  $format += array(
    'new_column' => FALSE,
  );

  $output = array();
  ksort($months);
  foreach ($months as $month => $weeks) {
    $string = '<h3 class="month">' . pgn_calendar_get_month_name($month) . ' ' . $year . '</h3>';
    $variables = array(
      'weeks' => $weeks,
      'format' => $format,
    );
    $string .= theme('pgn_pdfcalendar_main_week', $variables);
    $output[] = $string;
  }

  if ($format['new_column']) {
    $glue = '<br pagebreak="true" />';
  }
  else {
    $glue = '';
  }

  return implode($glue, $output) . $glue;
}

/**
 * Returns the HTML output for one week.
 *
 * @param array $variables
 *   An associative array containing:
 *   - weeks
 *   - format
 *
 * @ingroup main_calendar
 */
function theme_pgn_pdfcalendar_main_week($variables) {
  $weeks = $variables['weeks'];
  $format = (array) $variables['format'];

  $output = '';
  ksort($weeks);
  $first = 0;
  foreach ($weeks as $week => $days) {
    if ($first) {
      $output .= '<h4 class="week">' . 'Kalenderwoche ' . $week . '</h4>';
    }

    $variables = array(
      'days' => $days,
      'format' => $format,
    );
    $output .= theme('pgn_pdfcalendar_main_event', $variables);
    $first++;
  }
  return $output;
}

/**
 * Returns the HTML output of all events of a day.
 *
 * @param array $variables
 *   An associative array containing:
 *   - days
 *   - format
 *
 * @ingroup main_calendar
 */
function theme_pgn_pdfcalendar_main_event($variables) {
  $days = $variables['days'];
  $format = (array) $variables['format'];

  $format += array(
    'short' => TRUE,
  );

  $holiday_id = pgn_calendar_get_calendar_id('holiday');

  $output = '';
  ksort($days);
  foreach ($days as $hours) {
    ksort($hours);
    foreach ($hours as $events) {
      foreach ($events as $event) {

        $variables = array(
          'event' => $event,
          'format' => $format,
        );
        if ($event['calendar_id'] != $holiday_id) {
          $output .= theme('pgn_pdfcalendar_calendar_token', $variables);
          $output .= '<span class="date">' . theme('pgn_calendar_date', $variables) . '</span><br/>';
          $output .= '<blockquote>' . theme('pgn_pdfcalendar_event_text', $variables) . '</blockquote>';
        }
        else {
          $output = '<br/>';
          $output .= '<div class="holiday">';
          $output .= $event['title'];
          $output .= ' (' . theme('pgn_calendar_date', $variables) . ')';
          $output .= '</div>';
          $output .= '<br/>';
        }
      }
    }
  }
  return $output;
}

/**
 * Shows link.
 *
 * @param array $variables
 *   An associative array.
 *
 * @ingroup main_calendar
 */
function theme_pgn_pdfcalendar_links($variables) {
  return '<p class="link-text">Eine ständig aktualisierte und erweitere Version
    des Terminplans finden Sie unter:
    <br/>
    http://pirckheimer-gymnasium.de/termine<br/>
    <br/>
    Sie können sich auch eine aktualisierte Fassung des PGN-Kalenders
    ausdrucken:
    <br/>
    http://pirckheimer-gymnasium.de/termine-drucken
    </p>';
}

/**
 * Returns a colored output of the calendar token.
 *
 * @param array $variables
 *   An associative array containing:
 *   - event
 *   - format
 */
function theme_pgn_pdfcalendar_calendar_token($variables) {
  $event = $variables['event'];
  $format = (array) $variables['format'];

  $format += array(
    'color' => TRUE,
  );

  if ($format['color']) {
    $class = 'color';
    $style = 'style="background-color: ' . $event['calendar_color_hex'] . '"';
    $calendar_token = ' ' . $event['calendar_short'] . ' ';
  }
  else {
    $class = '';
    $style = '';
    $calendar_token = '[' . $event['calendar_short'] . ']';
  }

  return '<span class="calendar-short ' . $class . '" ' . $style . '>' . $calendar_token . '</span> ';
}

/**
 * Return a HTML table for the exam calendar.
 *
 * @param array $variables
 *   An associative array containing:
 *   - events
 *   - format
 *
 * @ingroup exam_calendar
 */
function theme_pgn_pdfcalendar_exam($variables) {
  $events = $variables['events'];
  $format = (array) $variables['format'];

  $output = '';
  ksort($events);
  foreach ($events as $year => $months) {
    ksort($months);
    foreach ($months as $month => $weeks) {
      $output .= '<h2 class="month">' . pgn_calendar_get_month_name($month) . ' ' . $year . '</h2>';

      $variables = array(
        'weeks' => $weeks,
        'format' => $format,
      );
      $output .= theme('pgn_pdfcalendar_exam_event', $variables);
    }
  }
  return $output;
}

/**
 * Returns one event for the event calendar.
 *
 * @param array $variables
 *   An associative array containing:
 *   - weeks
 *   - format
 *
 * @ingroup exam_calendar
 */
function theme_pgn_pdfcalendar_exam_event($variables) {
  $weeks = $variables['weeks'];
  $format = (array) $variables['format'];

  $format += array(
    'color' => TRUE,
  );

  ksort($weeks);
  foreach ($weeks as $days) {
    ksort($days);
    foreach ($days as $hours) {
      ksort($hours);
      foreach ($hours as $events) {
        foreach ($events as $event) {

          $cells = array();
          $cells[] = array(
            'data' => $format['color'] ? $event['calendar_short'] : '[' . $event['calendar_short'] . ']',
            'class' => 'calendar-short',
            'width' => $format['color'] ? 30 : 50,
          );
          if ($format['color']) {
            $cells[] = array(
              'data' => '',
              'class' => 'color',
              'width' => 20,
              'style' => 'background-color: ' . $event['calendar_color_hex'] . ';',
            );
          }
          $cells[] = array(
            'data' => $event['start']['day_word'],
            'class' => 'day-word',
            'width' => 25,
          );
          $cells[] = array(
            'data' => $event['start']['day'],
            'class' => 'day',
            'width' => 20,
          );

          $variables = array(
            'event' => $event,
            'format' => array(
              'show_inline_date' => TRUE,
            ),
          );
          $cells[] = array(
            'data' => theme('pgn_pdfcalendar_event_text', $variables),
            'class' => 'title',
            'width' => 440,
          );
          $rows[] = $cells;
        }
      }
    }
  }

  $variables = array(
    'rows' => $rows,
    'attributes' => array(
      'cellpadding' => 0,
      'cellspacing' => 2,
    ),
  );
  return theme('table', $variables);
}

/**
 * Returns one week as a HTML table.
 *
 * @param array $variables
 *   An associative array containing:
 *   - events
 *   - start
 *   - end
 *   - format
 */
function theme_pgn_pdfcalendar_week_table($variables) {
  $events = $variables['events'];
  $start = $variables['start'];
  $format = (array) $variables['format'];

  $format += array(
    'color' => TRUE,
  );

  $monday = $start['time_stamp'];
  $days = array();
  for ($i = 0; $i < 5; $i++) {
    $days[] = $monday;
    $monday = $monday + (24 * 60 * 60);
  }

  foreach ($days as $day) {
    $day = pgn_calendar_date_format($day);

    $rows[] = array(
      'data' => array(
        'first' => array(
          'data' => $day['date'],
          'class' => 'day',
          'colspan' => $format['color'] ? 3 : 2,
          'width' => 780,
        ),
      ),
    );

    if (isset($events[$day['year']][$day['month']][$day['week']][$day['week'] . ':' . $day['day_zero']])) {
      $day = $events[$day['year']][$day['month']][$day['week']][$day['week'] . ':' . $day['day_zero']];
    }
    else {
      $day = array(array('title' => '-'));
    }

    ksort($day);
    foreach ($day as $hours) {
      foreach ($hours as $event) {
        $cells = array();
        $cells[] = array(
          'data' => $format['color'] ? $event['calendar_short'] : '[' . $event['calendar_short'] . ']',
          'class' => 'calendar-short',
          'width' => $format['color'] ? 30 : 50,
        );

        if ($format['color']) {
          $cells[] = array(
            'data' => '',
            'class' => 'color',
            'width' => 20,
            'style' => 'background-color: ' . $event['calendar_color_hex'] . ';',
          );
        }
        $variables = array(
          'event' => $event,
          'format' => array('show_inline_date' => TRUE),
        );
        $cells[] = array(
          'data' => theme('pgn_pdfcalendar_event_text', $variables),
          'class' => 'title',
          'width' => 710,
        );
        $rows[] = $cells;
      }
    }
  }
  $variables = array(
    'rows' => $rows,
    'attributes' => array(
      'cellpadding' => 3,
      'cellspacing' => 3,
    ),
  );
  return theme('table', $variables);
}

/**
 * Return the semester title.
 *
 * @param array $variables
 *   An associative array containing:
 *   - semester
 *   - year
 *   - format
 */
function theme_pgn_pdfcalendar_semester_title($variables) {
  $semester = $variables['semester'];
  $year = $variables['year'];
  $format = (array) $variables['format'];

  $format += array(
    'short' => FALSE,
  );

  $school_year = $year . '/' . substr($year + 1, 2);
  switch ($semester) {
    case 1:
      return !$format['short'] ? '1. Halbjahr ' . $school_year : '1. HJ';

    case 2:
      return !$format['short'] ? '2. Halbjahr ' . $school_year : '2. HJ';

    case 3:
      return !$format['short'] ? 'Schuljahr ' . $school_year : '2. HJ';
  }
}

/**
 * @} End of "addtogroup theme_functions".
 */
