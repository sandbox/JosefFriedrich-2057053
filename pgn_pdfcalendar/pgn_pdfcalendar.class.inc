<?php

/**
 * @file
 * PGN TCPDF class.
 */

class PgnTcpdf extends TCPDF {

  public $headerLeftText = '';

  public $headerRightText = '';

  public $footerLeftText = '';

  public $footerRightText = '';

  public $renderBuild = array();

  public $fileName = 'Terminplan';

  public $pdfOutputMode = 'I';

  public $htmlOutputMode = FALSE;

  /**
   * Initializes the PDF class.
   */
  public function init() {
    $this->SetAuthor('Pirckheimer-Gymnasium, Nürnberg');

    $this->SetCreator('Pirckheimer-Gymnasium, Nürnberg');

    $this->SetTitle('PGN-Kalendar');

    $this->setPDFVersion('1.6');

    $this->setFontSubsetting(FALSE);

    $this->SetMargins(10, 17, 10);

    $this->SetHeaderMargin(10);

    $this->SetFooterMargin(13);

    $this->setListIndentWidth(5);

    $tagvs = array(
      'h2' => array(
        0 => array(
          'h' => 0,
          'n' => 0,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'h4' => array(
        0 => array(
          'h' => 1,
          'n' => 1,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'h3' => array(
        0 => array(
          'h' => 1,
          'n' => 2,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'h4' => array(
        0 => array(
          'h' => 1,
          'n' => 1,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'p' => array(
        0 => array(
          'h' => 0,
          'n' => 0,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'blockquote' => array(
        0 => array(
          'h' => 0,
          'n' => 0,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
      'div' => array(
        0 => array(
          'h' => 0,
          'n' => 0,
        ),
        1 => array(
          'h' => 0,
          'n' => 0,
        ),
      ),
    );

    $this->setHtmlVSpace($tagvs);

    $this->AddPage();
  }

  /**
   * Shows the header of the PDF.
   */
  public function Header() {
    $this->SetFont('pgnsans', '', 8);
    $width = $this->w - $this->original_lMargin - $this->original_rMargin;
    $this->Cell($width / 7 * 3, 0, $this->headerLeftText, 'B', 0, 'L');
    $this->Cell($width / 7 * 4, 0, $this->headerRightText, 'B', 0, 'L');

    $imgdata_color = base64_decode(
    'iVBORw0KGgoAAAANSUhEUgAAAUUAAABICAMAAABWQxcFAAAACXBIWXMAAAsTAAALEwEAmpwYAAAK
    T2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AU
    kSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXX
    Pues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgAB
    eNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAt
    AGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3
    AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dX
    Lh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+
    5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk
    5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd
    0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA
    4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzA
    BhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/ph
    CJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5
    h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+
    Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhM
    WE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQ
    AkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+Io
    UspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdp
    r+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZ
    D5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61Mb
    U2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY
    /R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllir
    SKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79u
    p+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6Vh
    lWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1
    mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lO
    k06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7Ry
    FDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3I
    veRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+B
    Z7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/
    0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5p
    DoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5q
    PNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIs
    OpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5
    hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQ
    rAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9
    rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1d
    T1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aX
    Dm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7
    vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3S
    PVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKa
    RptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO
    32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21
    e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfV
    P1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i
    /suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8
    IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADq
    YAAAOpgAABdvkl/FRgAAAwBQTFRFRFx4NVFu7Ozx/f391ujr6vP2i5SobHqTgoyisbXEur3L8PH1
    3d7mpKq7S2F9kpqulZ2wrbLCf4qg+fn70dPdeYWcdLjCwsXR9fb4ttfctrrIWrS9VmmEOlVyME5r
    Lk1qVa249aJPztHbSaq1y83YvcDNxcjUUmeCn8zT9/j5xt/kgL7GicHJ2NnicX6W+vz9WWyHZLK8
    8o8uYnKMmaG0XG6I6ervdYKZ+vr74OHoKkpozePnZXSO5ebsPFZz1dbgTau2nqW3p62+73oAyMrW
    4uTrM1BtUKy2o83Uq9HXksbNKElnYHCK9PT3IkZk9Jg+mJ+yiJKnOFNw29zkfIeeXK+5hY+lJkhm
    oKe419jh7nYB5+jts7fGQ6izP1h1xMfTwMPQZ3aQK0tpTmR/Y3ONm6O1aXiR2drjPld0NlJvRX2T
    HkRh730A//v4TYGWPFJw3+3wX3CKjper8/T34+Xrq7DAn6a4//z55PDzv8LPT7G7qa+/vtvgWK65
    PVdz////gICAgYGBgoKCg4ODhISEhYWFhoaGh4eHiIiIiYmJioqKi4uLjIyMjY2Njo6Oj4+PkJCQ
    kZGRkpKSk5OTlJSUlZWVlpaWl5eXmJiYmZmZmpqam5ubnJycnZ2dnp6en5+foKCgoaGhoqKio6Oj
    pKSkpaWlpqamp6enqKioqampqqqqq6urrKysra2trq6ur6+vsLCwsbGxsrKys7OztLS0tbW1tra2
    t7e3uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIycnJ
    ysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV1tbW19fX2NjY2dnZ2tra29vb3Nzc
    3d3d3t7e39/f4ODg4eHh4uLi4+Pj5OTk5eXl5ubm5+fn6Ojo6enp6urq6+vr7Ozs7e3t7u7u7+/v
    8PDw8fHx8vLy8/Pz9PT09fX19vb29/f3+Pj4+fn5+vr6+/v7/Pz8/f39/v7+////uaxT7AAAEt1J
    REFUeNrs3PlD2uifB/BAWjkNh1Asm8Mvggw9gK7KNYpC8elMPSpI1VYGNfHKzuzugNpt12ne//r3
    hwRIuESL3Z35zvPLOJBwvPrkOT6fT6Dwd/v2Rv1N8Lfin1dx6tIRo287xpHpeSyT+D/4fo7sgfGz
    Zr6b4rt37969e/eP/xnwwXy1iWM22dgY1Bos67dPd59G//7z8yfP+7XdRd25i+FBH9Wb1I5JBtqP
    rc0nWTZZDQPIJt0AviSTLJv0htSnfZZ6hWNvAJyzBQCwSW6gGPUB0P5L+5Msy7JMAQhIHgDXbLL1
    ESJsMgKgrB5RBkLJJMsmq6WRFF+/fv369cd//8+B9nR2kuV5uX/j+fngZt+zfv/hzS+rPe1//0P/
    SvzZoDdttA4Tau3HdmTG61+SyTXwQfAD8OSSXv8xRyYB4IiTrbZtKucCQuIugJDMpIDm+iEAoLk3
    BySinL9araaDQG3LDcAlirvaq8dF4RrAMUl7q9X0JBAQWa//LM8XRlH87bfffvvt9RBFALSdJZzS
    2/IkGhl81ucXszNfu9q//dd/68+Xvww490TWDjEF24855SoAH0WZ8dLkBWATPwHIMvk1wMxUDgE4
    qcoHhEgZMEucD4BLmAMAuMQSkIhGWyNNMLcAwJ2XJPVCSlES5wFwVk9pRxyK2wCWo/LOmBSBmIur
    XHQjcibr5dCznr6ZGaqomE7upOgHgLJw2FYsAIBbjACT62qnqv66ryrGRQ/6KGaMivUmUfuqjTT7
    KuJ0PTg2RWCOkbsYucrkbSf9ODMzVFGPNKriqWg3Kp6KdiDOqwPYjs2JEClje6+MERTzYcWqXtDM
    kamvYnG9NkZFnDOyEZGr3T6hPVudGaooL2bueEXDz4eMimcmJ2LR9rcHQsRV4k+mNMWSQZE2KlbM
    FmYaQEqxnpO+inF+ZZyKCF/ox8a8qdj1fOLx48eX3cuin2eHKirk+k59kXZM8smMTjHmrIoTgIOS
    OguFkGxhmDX1bxfvjtjt9qM0XwISLHXj8XhuljuKDo9wCMAmHO4QDwALt2/zeIo7wKHoRszpbU8/
    d1OccjqdTqfzKhXrPvSU6L++8dUTz94+ef/166snPz82Pv5kdaiiHN0cWbFeZ5i6GF1BS5FQEmWi
    7ADMdbbTqUP5iuDW/nYTmRBCSKVSAhJJmRBChIiuL6Y4K4A4lVlRFU0mQsj6PnBooiRKpmq4l+KK
    kr+4uLigqOjSxKHhesssdq5pOWlYJD56Mrv6ZmZmZubN7PunxqFxuKJC9kdWzEfLu037JdqK/KLX
    z+bLiZ6+GI/WV1p9sXju8/mcXqEEJKLMedbpdE53FA9wxlziIF9FSL2iL8JrTueOAzgkSa8/md+N
    AYjFYrFYbOouihyXz+fzHFeRiZw80h9bayvmKwHDZLzasZr55SfDKun56lDFiuS427iIjqL4CYB/
    bxKYMo6LTR9hHX1nl66VTmUZRfELPEKkpdg1Lnr3CoAjSTEMQ53dRTGf1y3oTKf69U600upBaQOi
    YWE4M/vU+ORQRYVs322O1isWALyUzwBY+DmdYhmf1v2jzNGVZVxxXlioywGKWe4EmLam0+l0vHlP
    RUXhiE13cFkbGfPKiuGiNU7EM+/1Y+PnrzNDFTkm9U2KNMtsAoX1skER/vXCaIo4Y87rXgxQxCJl
    vte4aFBUKozuVSLalzPpu+L0qzdda+vZnw3zy5uhigppfpMiqmQFWK5fqP+wU7Sq6GDJ4UhXNG7k
    uBwYqLjLh8egqBDdimaN4tQLXT9c/vxL9z7vzRN9SOftal/F9rtwVPabFCdzNgAFkYlMJ8wey5Sq
    iA8KtdajyLx0Op07VwbFZYpIm21FJZx1OneybUVbbnIciiZLz8BYYac6j12+6tktz3z9PGDJqFOs
    5wcsmgYp7gjtK0CLRmwtAEA45wUAd0WkmLxwBoRyVgCoiYsxNPfUabC5NQckonylUqnw861oRJO/
    AmDZsgIIiTcAjoVKpVIhTAyBLRcArAjpcShykm6zvCT3XIN/zH7taas/DpheOopyNdmaqrj6+SiK
    l0FfO2AX9AG4sjkBIGZXtyfOwq71NJwBHDX14j60HeC8pg5I5zYzgEAwGAwGa3PAgW0HwE4wBuDc
    tgbAUVsDEA4Gg8Fg8AsNs20HAKbspbEo1nUXXNqkKEqe07/wi9VexdlnA5g7isRtay/iSXUUxf/P
    se5bFfNcqHN0lXT3zse9F/TXrzP6EO2TvnM0aYJtUeXzob++om6S2iWKopjiA/cmbUZdZHam73qR
    TOC60xnj/1J90UoURSGng8MNtzSDYqK9ozSOEX9JxfpO17goB24ZFkdURLC9ozSd/cUVOUa30T2W
    FYWrOwesqe+qSM/LA3IHAxUTY8rrTSW+r6J+FIxFK92Ty/uZ+yvii9x6L3l+BEXabpGY+VMHkNn1
    H6iPHVmukaqm24PMl3g6DOAo7lU3IcG4tsQIx6utbYnPmmSk+YkYgAnrNAC3X10PeSw+ADdxf+vy
    i+3Gm2NR1G+knfV81xf+feZbFHFm6p876KtIV0XFkmZy7Brg3lKX6g6J98FZEamWkCUn2gCc5tSt
    B1x72uToXVczMUDRxDf8FuYiBdBS3QGgYVKDutYtO4BqLtcKT5Z4kR2HopzU7VNsptGm6JEVS1y+
    b+6gr6J7vbEMZKy5MyDGmkIqUhNw1imTFk29oii5BmCfyGoAwM2rk+Mmw1LqB9/h6gEAmQ8xgGYp
    B4BjRVUsixEAVu5isRV+keuLY1DkDLNn2tS9YXumn6Jn+rchioiTvrmDfooHlDogpyjBB0SEMwA7
    nOQAnJxfE8INqaqKJgtZ0CsGhGJVjTQU162d7t1P0UulTeo2KSYtNsbQF2VDiipLcYqiENeg6OHM
    rYw9ir72u1X0uYN+itc5LRpRboQAxMUgEBdtAJyyO63FYs4YD6kB2BdsLOPQKe6StZpYA4CCuu8e
    pljTvuOcsHD2rYqcTNg59IQXSaH/cnHm64+ff+/TPuuCOj2K6m6oJ3fQT9Gbu9F/mJdclD4SjtVo
    z0JQtAHAFectCTUA++KhLVfoKE5JLNYqaQAokYvIcMV6NsrS6hd+uXQ/RR/hCSGEEJk6KRpSS746
    pyiKYroepPi5/9rgh9khii/rXDt3YB6qeMYbshRo5poNOaQquh1cHACKYiCsKUYgMZdtxTBfBhpq
    xHVXrEyYhyleTE/wPgAxZhH3vKJTrqbL5XKdFgPLXfURG+pXM+kv8p++VVHdVGoBiqGKG6YQEE5u
    bJwsnQOAWTLxu1rksQlLPQVgiZoKaIp27Ocm24pNIQC4RTsA0IX6OlWYHqJ4GRJcAA6FhfsqDmxe
    7eua7GNVVAdbdX1/MExxnoSBErvYoLTw8zXRkg1OuYlr0QZkK2kctRUdDLOpKdIscwn4iDayrlnl
    HFsarGimJZYGrOTluBXLJP8gimiS3txBP0WLoEXYrYI6YTgrcbQVl/NxYFK040tbEYX1G03RZzo+
    962EmfY+zLck5EsDFVMo8z5sMosYr6Ij3UIcu+JBpzO2cwf9ZxdtQLbyfRRhuUhhiXLoFR2MhG0+
    BMDNm3ieJya5U+PmFefbikvKVZdiWHAhIJyOV/Eo2qmLGLci3KQnd9BPcTJnHaboEYPmShV6RRTW
    7Z/4EIDF+qTN46m5+M4iJ1unzG3FfLZLcUpiYTXtjFMxHJflAXVeBsVH04/bLTG6opnp5A5eDlY8
    57QCgP6Ky3n/jRgxKpqphouEgJda8uiSYdoxAAdTbyvGifqKaT6sKaJMDqUNjE0xWzzhTPrtjMkz
    KLz4vtPePBtdEYVOZ/QP2QHGte1t33ERsNSjjMOoiG0S5UJAobXUtArt1VKEJOmW4um6GwBSDGVu
    KYblRbkwBsWEYy18vduoE7krPPFphL3L7F0UL9s1F63cQV9FJyWUnamdSaq/oofnvWgpflIVzYyJ
    CwHznDbgHolWlKoRZyo7Wedt7XHxgJInr1KhY7U0wKukgAzL13cALEbvpZg9Ozs7O7OcNFimzhEi
    95QiG3aAfwwIdd9JEcXu3EH/yNjLeZ5wRGAKan2fk2hJXic/AeCA4iMAIus2AIU9dfR2C3IIO6RV
    s+ugGAREwcSZROYaAB2lHABQYkS5IhBvDACqXArAxN4JACSl++1dTEQtTK9wXL5PMbdCvIOiEfdW
    jCW7cgcDorSJ8L6rcNga2mJzH1p/ZAEgVJoCYA6kACwH1N2JY27OAXOgHVdeOYxlfLYFV+FIHWND
    4YSWm912FbWymZ1SBkBKPckXGkt8safp0/x4tDoORTXkpssd/OUyBj2toiu4NNYy3V+R3ujkDiL/
    EoocldKXOo1FERFd7oAeXTG14gTtXEn9+RQNCWr6+ZuxKOK4c03XRlcsCyxiUcH951NUiL6K6u3q
    eBQPK+3OmMyMrLi7J2Ga2Wv+GRWrgxaM36BozB2MqugMhECHAtk/oaJhevn9zcx4FEPtdVWFnV76
    y88uxnrkAWn9uyvC3+mMk5Z+iiVb6+7XD55zADi0adPKlacEACu2YLBmD2tx7ETEFmifmQJAf6kF
    gzX7itYHDmpqlZ3NByBsCwZr9lCngIGec1t3T1ceTtGYeflhdkyKK+27kjgpyvVRPBYFbQvf/NUF
    AIvrWqCrtrWkhmR4URQFSk3ob1ICpyks7X0BkJF4QRRFk3a3cEQgoiiK4q9eAHHt1F3tnyB7zOcI
    WZf9jgdTNOT1+6+776GollJp+dt+ASSLqZUcd+1tA8AJr8Vtg4JFTQtYPJMLVirHmgFsMlxrm2Xh
    AwAyrNy8nnQlhXxYDUdIk8VisbgfBuAX4uqpG5sAcCWJUduHlQIlniUeSjGf9+nXOqtjUnTWe28f
    NijKdeIZqphzAUCKFV0ANpl8/eLcqOgDQKfFOABEeF3C3p87BYCsJH4CgLSYNANAIM97HkrRmNjv
    G5C4j2L7RpCBirvcYuJ2RRTFJQCbDOMVrUbFEACUTMymqkh3K2JBjANY4SqH2liwZXkwRY660s0v
    z1fHpHhFcUMV+YCaxb9NMSIs0sAmQ/mYC2ev4ku1DK6vok04BrAttp4qHjcfTNFYH9+v4uleinCR
    oYpCeI5v0KP0RQuATabucKvVJEZFO9/AIMXtnB9AXLQ++EpHURROXxqKP3p/2uB+imaGG6oYwIkQ
    vFXRkcwVVUVziqpnu8fFVFK8GTQupiSxBiAp7H8Pxa5K7Kc9jPdTNN483Kt4BLvQoIcoNuHIBtlc
    OqMqHqC5XtYrWrfdXiq/raUMGJfb7XY3dwD4c244sjZJ9AKYYnkPgMDp/v5+IfKAinlDnQmevTfe
    ULn65tG9FB1SZbgivcHbBysSiqHyfNROo6W4Vq9nO4oVXhRFUbJrYSROEARB2KsB8BOKoTie/QIA
    sShvA5DeEkUhd/aAigpHGdb1j9/OtO6QXp2dnXnxCPdSxCQZroigMD9YkWcai9EK5U60FVFeb+r6
    4ulRxD5RF6s0gAiJzpXm5uYCZgB+wjQWo7KajMiwwg2AFXvk0CrEH1JRkVljVO/z0xdPXr1//+rJ
    87d/fL5TPtpQDMTKwxXpRf5o6LhIH1Lq8KgqZuvUlXFchI8SbH3HRTpCqcNjQzxtJ8EfVvGCzHfv
    jhLTjx9PJ+6c1Tc0GxmuiJpwDPfQ2cXGM5ttReyuu7rmaLjF+IA5elKIZgCk23P0QysqCmmMFmO+
    kyLdkIcr0kk+dKopCtqz16JhjmbkUEfReUGZ40ZFu7AxQHGZ4s7V9eJ3U1SIFLrdMPFUdxvC7Yqw
    G0sIehThEf3udfVXWkQtXrygltm21osWsdhRxO76QtqoaBPmB60X54UggFCltcf9DooXsnKauAXx
    0fPZYRXJfdqSabhiJpnf4LcBwJXT7rq15Bb0iq6cV6e4k5eSxKAYF6sD1ovYzTUBYEn0fzdFReFI
    ozSsH/74YnV12D0G/VpAX4vRRxHXhFN/bGKFq9gAoCjnzw07QH5Rpwgrn5fbinTMWSVyST0qAZqm
    aVq/AxSPAaBUIc0MgJhXHGkf/fHjx48f9b9555NN8h2awB/bN/tPt49+evXLL92/eaeVDMji7kB7
    i6B7eV3h7lnuizaPEy1f5RbIcXl3g+cXAAATW+rGNMtdLAObFKcqnudlcgQgEyUSy0pyjvukWiss
    y7KslAaQ3lKX4iuyWixxw4lRbznOCPL2KIr/UFvn9xedx3dsjY3dwFTP6z5+9sOL589fdLW35ZPW
    WYN/pCx00nnxeV15vpfR+r1NYrSzPWxF4PMNjfqUUkuIMhYmBEzPt9ZiZUkKA8hYJEmSovNldcwr
    SVFJkiSJiQOYoNQfdphektSyNV+aIoLMVMMj7QDH0eg+vxeaSIz9bTIZuvcvZ8i33Pofekr7KzNF
    A8i00kO0dnwmk8m0HwSdabXeUwHAvBJyTo+4j/67jSN79Xe7Y/vnALqlvD2MaoDQAAAAAElFTkSu
    QmCC');
    // Dimensions: 325 x 72.
    // height = 72 / 325 * width.
    // $this->Image($imgdata, $x, $y, $w, $h, $type, $link, FALSE, $resize, $dpi, $palign);
    $this->Image('@' . $imgdata_color, 0, 10, 30, 72 / 325 * 30, 'PNG', '', FALSE, FALSE, 300, 'R');
  }

  /**
   * Show the footer for the PDF.
   */
  public function Footer() {
    $this->SetFont('pgnsans', '', 8);
    $width = $this->w - $this->original_lMargin - $this->original_rMargin;
    $this->Cell($width / 2, 0, 'Seite ' . $this->getAliasNumPage() . ' von ' . $this->getAliasNbPages(), 'T', 0, 'L');
    $this->Cell($width / 2, 0, $this->footerRightText, 'T', 0, 'R');
  }

  /**
   * Returns the rendered PDF.
   */
  public function pgnOutput() {
    if (!$this->htmlOutputMode) {
      $this->writeHTML(render($this->renderBuild));
      $this->lastPage();
      ob_clean();
      $this->Output($this->fileName . '.pdf', $this->pdfOutputMode);
    }
    else {
      return $this->renderBuild;
    }
  }
}
