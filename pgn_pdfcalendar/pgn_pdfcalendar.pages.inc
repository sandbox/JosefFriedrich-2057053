<?php

/**
 * @file
 * Page callbacks.
 */

/**
 * Page callback: Kalender drucken.
 *
 * @see pgn_pdfcalendar_menu()
 */
function pgn_pdfcalendar() {

  $build = array();

  $build['#attached'] = array(
    'css' => array(drupal_get_path('module', 'pgn_pdfcalendar') . '/pgn_pdfcalendar.css'),
  );

  $build['main'] = array(
    '#type' => 'fieldset',
    '#title' => 'Haupt-Terminplan',
  );

  $build['main']['frame'] = array(
    '#theme' => 'pgn_pdfcalendar_form_frame',
    '#image' => array(
      '#theme' => 'image',
      '#path' => drupal_get_path('module', 'pgn_pdfcalendar') . '/images/main.png',
    ),
    '#form' => drupal_get_form('pgn_pdfcalendar_select_main'),
  );

  $build['week'] = array(
    '#type' => 'fieldset',
    '#title' => 'Wochen-Terminplan',
  );

  $build['week']['frame'] = array(
    '#theme' => 'pgn_pdfcalendar_form_frame',
    '#image' => array(
      '#theme' => 'image',
      '#path' => drupal_get_path('module', 'pgn_pdfcalendar') . '/images/week.png',
    ),
    '#form' => drupal_get_form('pgn_pdfcalendar_select_week'),
  );

  $build['exam'] = array(
    '#type' => 'fieldset',
    '#title' => 'Oberstufen-Terminplan',
  );

  $build['exam']['frame'] = array(
    '#theme' => 'pgn_pdfcalendar_form_frame',
    '#image' => array(
      '#theme' => 'image',
      '#path' => drupal_get_path('module', 'pgn_pdfcalendar') . '/images/exam.png',
    ),
    '#form' => drupal_get_form('pgn_pdfcalendar_select_exam'),
  );

  return $build;
}

/**
 * Submit function for pgn_pdfcalendar().
 *
 * @see pgn_pdfcalendar()
 */
function pgn_pdfcalendar_submit(&$form_state, $values = array()) {
  $values['calendars'] = array_filter($form_state['values']['calendars']);
  $values['format']['color'] = !$form_state['values']['color'];
  drupal_goto('termine-drucken/' . _pgn_calendar_path_encode($values));
}

/**
 * Page callback: PDF calendar path handler.
 *
 * @see pgn_pdfcalendar_menu()
 */
function pgn_pdfcalendar_printer($path) {
  $value = _pgn_calendar_path_decode($path);

  $calendars = $value['calendars'];
  if (empty($calendars)) {
    $calendars = array('pgn', 'q11', 'q12', 'holiday');
  }
  $calendar_ids = pgn_calendar_get_calendar_ids($calendars);
  $calendars = pgn_calendar_get_calendars($calendar_ids);

  $events = pgn_calendar_event_query(array_keys($calendars), $value['query']);

  if (empty($value['query'])) {
    $value['query'] = array();
  }
  $query = $value['query'];

  if (empty($value['format'])) {
    $value['format'] = array();
  }
  $format = $value['format'];

  switch ($value['mode']) {
    case 'main':
      $events = _pgn_pdfcalendar_ksort($events);
      return pgn_pdfcalender_pdf_main($calendars, $events, $query, $format);

    case 'week':
      $events = pgn_pdfcalendar_event_splitter($events);
      $events = _pgn_pdfcalendar_ksort($events);
      return pgn_pdfcalender_pdf_week($calendars, $events, $query, $format);

    case 'exam':
      $events = _pgn_pdfcalendar_ksort($events);
      return pgn_pdfcalender_pdf_exam($calendars, $events, $query, $format);
  }
}

/**
 * Converts a German date string dd.mm.yyyy to a yyyy-mm-dd date string.
 *
 * @param string $date
 *   A date in format dd.mm.yyyy
 *
 * @return string
 *   A date string in format yyyy-mm-dd
 */
function pgn_pdfcalendar_text_date($date) {
  $output = explode('.', $date);
  return $output[2] . '-' . $output[1] . '-' . $output[0];
}

/**
 * @defgroup main_calendar Main calendar
 * @{
 * Haupt-Terminplan
 */

/**
 * Form constructor for the main calendar.
 *
 * @see pgn_pdfcalendar_select_main_validate()
 * @see pgn_pdfcalendar_select_main_submit()
 */
function pgn_pdfcalendar_select_main($form, &$form_state) {

  $form['semester'] = _pgn_pdfcalendar_semester_select();

  $form['advanced_settings'] = pgn_pdfcalendar_advanced_settings();

  $form['advanced_settings']['settings']['new_column'] = array(
    '#type' => 'checkbox',
    '#title' => 'Für jeden Monat einen neue Spalte beginnen',
    '#default_value' => FALSE,
  );

  $form['advanced_settings']['settings']['display_description'] = array(
    '#type' => 'checkbox',
    '#title' => 'Die Erklärungen am Ende des Terminplans ausblenden',
    '#default_value' => FALSE,
  );

  $textfield = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
  );

  $form['advanced_settings']['settings']['main_date_start'] = array(
    '#title' => 'Anfang',
    '#description' => 'Format: TT.MM.JJJJ',
  ) + $textfield;

  $form['advanced_settings']['settings']['main_date_end'] = array(
    '#title' => 'Ende',
    '#suffix' => '<div class="clearfix"></div>',
  ) + $textfield;

  $form['advanced_settings']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Druckversion aufrufen',
  );

  return $form;
}

/**
 * Form validation handler for pgn_pdfcalendar_select_main().
 *
 * @see pgn_pdfcalendar_select_main()
 */
function pgn_pdfcalendar_select_main_validate($form, &$form_state) {
  _pgn_pdfcalendar_validator($form_state['values']['main_date_start'], 'main_date_start');
  _pgn_pdfcalendar_validator($form_state['values']['main_date_end'], 'main_date_end');
}

/**
 * Form submission handler for pgn_pdfcalendar_select_main().
 *
 * @see pgn_pdfcalendar_select_main()
 */
function pgn_pdfcalendar_select_main_submit($form, &$form_state) {
  $values = array();
  $values['mode'] = 'main';

  list($semester, $year) = explode(':', $form_state['values']['semester']);

  $query = pgn_pdfcalendar_get_semester_period($semester, $year);

  $values['query']['semester'] = $semester;
  $values['query']['year'] = $year;
  $values['query']['start'] = $query['start'];
  $values['query']['end'] = $query['end'];
  $values['format']['new_column'] = $form_state['values']['new_column'];
  $values['format']['display_description'] = $form_state['values']['display_description'];

  if (!empty($form_state['values']['main_date_start'])) {
    $values['query']['start'] = pgn_pdfcalendar_text_date($form_state['values']['main_date_start']);
    unset($values['query']['semester']);
  }

  if (!empty($form_state['values']['main_date_end'])) {
    $values['query']['end'] = pgn_pdfcalendar_text_date($form_state['values']['main_date_end']);
  }

  pgn_pdfcalendar_submit($form_state, $values);
}

/**
 * Shows links to the exam calendar.
 *
 * @param array $calendars
 *   Calendars array.
 *
 * @return array
 *   A render array.
 */
function pgn_pdfcalendar_printer_link_view($calendars = array()) {

  $build['printer_heading'] = array(
    '#markup' => '<hr><h3>Druckversion</h3>',
  );

  $links = l(t('1. Halbjahr'), pgn_pdfcalendar_printer_link('exam', $calendars, 1)) . '<br/>';
  $links .= l(t('2. Halbjahr'), pgn_pdfcalendar_printer_link('exam', $calendars, 2)) . '<br/>';

  $build['printer_markup'] = array(
    '#markup' => $links,
  );

  return $build;
}

/**
 * Returns a link to a PDF calendar.
 *
 * @param string $mode
 *   The calendar view mode, 'main', 'exam' or 'week'.
 * @param array $calendars
 *   Calendars to show in the PDF.
 * @param int $semester
 *   The semester: 1, 2 or 3.
 *
 * @return string
 *   HTML output.
 */
function pgn_pdfcalendar_printer_link($mode = 'main', $calendars = array(), $semester = 3) {
  $values['mode'] = $mode;

  $year = pgn_pdfcalendar_get_semester_year();
  $semester = pgn_pdfcalendar_get_semester_period($semester, $year);

  $values['query']['start'] = $semester['start'];
  $values['query']['end'] = $semester['end'];

  foreach ($calendars as $calendar) {
    $values['calendars'][$calendar['token']] = $calendar['token'];
  }

  global $base_url;
  return $base_url . '/termine-drucken/' . _pgn_calendar_path_encode($values);
}

/**
 * Returns the rendered PDF of the main calendar.
 *
 * @param array $calendars
 *   Calendars to show in the PDF.
 * @param array $events
 *   Events for the Google calendar API.
 * @param array $query
 *   Query options.
 * @param array $format
 *   Format options.
 *
 * @return string
 *   The rendered PDF.
 *
 * @ingroup main_calendar
 */
function pgn_pdfcalender_pdf_main($calendars = array(), $events = array(), $query = array(), $format = array()) {
  $build = array();
  $build['css'] = array(
    '#markup' => pgn_pdfcalendar_css_load('main'),
  );

  $build['title'] = array(
    '#prefix' => '<h1>',
    '#markup' => 'Terminplan',
    '#suffix' => '</h1>',
  );

  $build['main'] = array(
    '#theme' => 'pgn_pdfcalendar_main',
    '#events' => $events,
    '#format' => $format,
  );

  if (!$format['display_description']) {
    $build['explanation'] = array(
      '#theme' => 'pgn_pdfcalendar_explanation',
      '#calendars' => $calendars,
      '#format' => $format,
    );

    $build['summary'] = array(
      '#theme' => 'pgn_pdfcalendar_calendar_summary',
      '#calendars' => $calendars,
    );

    $build['online'] = array(
      '#theme' => 'pgn_pdfcalendar_links',
    );
  }

  _pgn_pdfcalendar_tcpdf_init();
  $pdf = new PgnTcpdf('L', 'mm', 'A4', TRUE, 'UTF-8', FALSE);
  $pdf->headerLeftText = 'Terminplan des Pirckheimer-Gymnasiums Nürnberg';

  $variables = array(
    'query' => $query,
    'format' => array(
      'plain' => TRUE,
      'label' => FALSE,
    ),
  );
  $pdf->headerRightText = theme('pgn_pdfcalendar_period', $variables);

  $variables = array(
    'calendars' => $calendars,
    'format' => array(
      'plain' => TRUE,
      'label' => TRUE,
    ),
  );
  $pdf->footerRightText = theme('pgn_pdfcalendar_updated', $variables);
  $pdf->init();
  $pdf->SetFont('pgnsans', '', 12);
  $pdf->setEqualColumns(4, 66);
  $pdf->renderBuild = $build;

  $variables = array('calendars' => $calendars, 'query' => $query);
  $pdf->fileName = theme('pgn_pdfcalendar_file_name', $variables);
  $pdf->pgnOutput();
}

/**
 * Returns the rendered PDF of the exam calendar.
 *
 * @param array $calendars
 *   Calendars to show in the PDF.
 * @param array $events
 *   Events for the Google calendar API.
 * @param array $query
 *   Query options.
 * @param array $format
 *   Format options.
 *
 * @return string
 *   The rendered PDF.
 *
 * @ingroup exam_calendar
 */
function pgn_pdfcalender_pdf_exam($calendars = array(), $events = array(), $query = array(), $format = array()) {
  $build = array();
  $build['css'] = array(
    '#markup' => pgn_pdfcalendar_css_load('exam'),
  );

  $build['title'] = array(
    '#prefix' => '<h1>',
    '#markup' => 'Termin- und Schulaufgabenplan (' . theme('pgn_pdfcalendar_calendar_short', array('calendars' => $calendars)) . ')',
    '#suffix' => '</h1>',
  );

  $build['table'] = array(
    '#theme' => 'pgn_pdfcalendar_exam',
    '#events' => $events,
    '#format' => $format,
  );

  $build['summary'] = array(
    '#theme' => 'pgn_pdfcalendar_calendar_summary',
    '#calendars' => $calendars,
  );

  _pgn_pdfcalendar_tcpdf_init();
  $pdf = new PgnTcpdf('P', 'mm', 'A4', TRUE);
  $pdf->headerLeftText = 'Terminplan des Pirckheimer-Gymnasiums Nürnberg';

  $variables = array(
    'query' => $query,
    'format' => array(
      'plain' => TRUE,
      'label' => FALSE,
    ),
  );
  $pdf->headerRightText = theme('pgn_pdfcalendar_period', $variables);

  $variables = array(
    'calendars' => $calendars,
    'format' => array(
      'plain' => TRUE,
      'label' => TRUE,
    ),
  );
  $pdf->footerRightText = theme('pgn_pdfcalendar_updated', $variables);

  $pdf->init();
  $pdf->SetFont('pgnsans', '', 12);
  $pdf->renderBuild = $build;

  $variables = array(
    'calendars' => $calendars,
    'query' => $query,
  );
  $pdf->fileName = theme('pgn_pdfcalendar_file_name', $variables);
  $pdf->pgnOutput();
}

/**
 * Returns the rendered PDF of the week calendar.
 *
 * @param array $calendars
 *   Calendars to show in the PDF.
 * @param array $events
 *   Events for the Google calendar API.
 * @param array $query
 *   Query options.
 * @param array $format
 *   Format options.
 *
 * @return string
 *   The rendered PDF.
 *
 * @ingroup week_calendar
 */
function pgn_pdfcalender_pdf_week($calendars = array(), $events = array(), $query = array(), $format = array()) {

  $start = pgn_calendar_date_format($query['start']);
  $end = pgn_calendar_date_format($query['end']);

  $build['css'] = array(
    '#markup' => pgn_pdfcalendar_css_load('week'),
  );

  $build['title'] = array(
    '#prefix' => '<h1>',
    '#markup' => 'Wochenplan (Kalenderwoche ' . $query['week'] . ')',
    '#suffix' => '</h1>',
  );

  $build['table'] = array(
    '#theme' => 'pgn_pdfcalendar_week_table',
    '#events' => $events,
    '#start' => $start,
    '#end' => $end,
    '#format' => $format,
  );

  $build['explanation'] = array(
    '#theme' => 'pgn_pdfcalendar_explanation',
    '#calendars' => $calendars,
    '#format' => $format,
  );

  _pgn_pdfcalendar_tcpdf_init();
  $pdf = new PgnTcpdf('L', 'mm', 'A4', TRUE);
  $pdf->headerLeftText = 'Terminplan des Pirckheimer-Gymnasiums Nürnberg';

  $variables = array(
    'query' => $query,
    'format' => array(
      'plain' => TRUE,
      'label' => FALSE,
    ),
  );
  $pdf->headerRightText = theme('pgn_pdfcalendar_period', $variables);

  $variables = array(
    'calendars' => $calendars,
    'format' => array(
      'plain' => TRUE,
      'label' => TRUE,
    ),
  );
  $pdf->footerRightText = theme('pgn_pdfcalendar_updated', $variables);

  $pdf->init();
  $pdf->SetFont('pgnsans', '', 12);
  $pdf->renderBuild = $build;
  $variables = array(
    'calendars' => $calendars,
    'query' => $query,
  );
  $pdf->fileName = theme('pgn_pdfcalendar_file_name', $variables);
  $pdf->pgnOutput();
}

/**
 * @} End of "defgroup main_calendar".
 */

/**
 * @defgroup week_calendar Week calendar
 * @{
 * Wochen-Terminplan
 * @}
 */

/**
 * Form constructor for the week calendar.
 *
 * @see pgn_pdfcalendar_select_week_submit()
 */
function pgn_pdfcalendar_select_week($form, &$form_state) {
  $form['week'] = array(
    '#type' => 'select',
    '#title' => 'Kalenderwoche',
    '#options' => _pgn_pdf_calendar_options_weeks(),
    '#default_value' => _pgn_pdf_calendar_options_weeks_default(),
  );

  $form['advanced_settings'] = pgn_pdfcalendar_advanced_settings();

  $form['advanced_settings']['settings']['calendars']['#default_value'] = array('pgn', 'teacher', 'q11', 'q12', 'holiday');

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Druckversion aufrufen',
  );

  return $form;
}

/**
 * Form submission handler for pgn_pdfcalendar_select_week().
 *
 * @see pgn_pdfcalendar_select_week()
 */
function pgn_pdfcalendar_select_week_submit($form, &$form_state) {
  $values = array();
  $values['mode'] = 'week';
  $array = explode(':', $form_state['values']['week']);

  $cw = _pgn_pdfcalendar_get_calendar_week($array[0], $array[1]);

  $values['query']['week'] = $array[0];
  $values['query']['year'] = $array[1];
  $values['query']['start'] = $cw['mon']['date_iso'];
  $values['query']['end'] = $cw['mon2']['date_iso'];

  pgn_pdfcalendar_submit($form_state, $values);
}

/**
 * Returns an array with calendar weeks.
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [33:2013] => KW 33 2013 (Mo 12.8. - Fr 16.8.)
 *     [34:2013] => KW 34 2013 (Mo 19.8. - Fr 23.8.)
 *     [35:2013] => KW 35 2013 (Mo 26.8. - Fr 30.8.)
 *     [36:2013] => KW 36 2013 (Mo 2.9. - Fr 6.9.)
 *     [37:2013] => KW 37 2013 (Mo 9.9. - Fr 13.9.)
 *     [38:2013] => KW 38 2013 (Mo 16.9. - Fr 20.9.)
 *     [39:2013] => KW 39 2013 (Mo 23.9. - Fr 27.9.)
 *     [40:2013] => KW 40 2013 (Mo 30.9. - Fr 4.10.)
 *     [41:2013] => KW 41 2013 (Mo 7.10. - Fr 11.10.)
 *     [42:2013] => KW 42 2013 (Mo 14.10. - Fr 18.10.)
 * )
 * @endcode
 *
 * @param int $time
 *   The Unix timestamp.
 *
 * @return array
 *   An array of calendar weeks.
 */
function _pgn_pdf_calendar_options_weeks($time = '') {
  if (empty($time)) {
    $time = time();
  }

  $week = 7 * 24 * 60 * 60;
  $options = array();
  for ($i = 0; $i < 10; $i++) {

    $calendar_week = date('W', $time);
    $year = date('Y', $time);
    $cw = _pgn_pdfcalendar_get_calendar_week($calendar_week, $year);
    $time = $time + $week;

    $output = 'KW ' . $calendar_week . ' ' . $cw['mon']['year'];
    $output .= ' (' . $cw['mon']['date_short'] . ' - ';
    $output .= $cw['fri']['date_short'] . ')';
    $options[$calendar_week . ':' . $year] = $output;
  }

  return $options;
}

/**
 * Return an array to deliver the default option for the actual week.
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [0] => 33:2013
 * )
 * @endcode
 *
 * @param int $time
 *   The Unix timestamp.
 *
 * @return array
 *   An array to deliver the default option for the actual week.
 */
function _pgn_pdf_calendar_options_weeks_default($time = '') {
  if (empty($time)) {
    $time = time();
  }

  // Thu, Fri, Sat, Sun show the next week.
  if (date('w', $time) > 3) {
    $time = $time + 7 * 24 * 60 * 60;
  }

  return array(date('W', $time) . ':' . date('Y', $time));
}

/**
 * Returns to a given calendar week number the monday, friday and sunday.
 *
 * http://www.contentmanager.de/magazin/artikel_417.html
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [mon] => Array
 *         (
 *             [date_time] => 2013-08-26T00:00:00+02:00
 *             [time_stamp] => 1377468000
 *             [date_iso] => 2013-08-26
 *             [year] => 2013
 *             [month] => 8
 *             [week] => 35
 *             [month_word] => August
 *             [day] => 26
 *             [day_zero] => 26
 *             [day_word] => Mo
 *             [hour] => 00
 *             [minute] => 00
 *             [has_time] => 1
 *             [date] => Mo 26. August 2013
 *             [date_short] => Mo 26.8.
 *             [time] => 00:00
 *         )
 *
 *     [fri] => Array
 *         (
 *             [date_time] => 2013-08-30T00:00:00+02:00
 *             ...
 *             [time] => 00:00
 *         )
 *
 *     [sun] => Array
 *         (
 *             [date_time] => 2013-09-01T00:00:00+02:00
 *             ...
 *             [time] => 00:00
 *         )
 *
 * )
 * @endcode
 *
 * @param int $calendar_week
 *   The calendar week.
 * @param int $year
 *   The year.
 *
 * @return array
 *   Monday, friday and sunday as keys in a array (see above).
 */
function _pgn_pdfcalendar_get_calendar_week($calendar_week, $year) {
  $wdays = array(1, 0, -1, -2, -3, 3, 2);
  // W: 0 -> So, 1 -> Mo.
  $w = date('w', mktime(0, 0, 0, 1, 1, $year));
  $days = ($calendar_week - 1) * 7 + $wdays[$w];

  $output = array();
  $dsd = array('mon' => 0, 'fri' => 4, 'sun' => 6, 'mon2' => 7);
  foreach ($dsd as $key => $value) {
    $output[$key] = pgn_calendar_date_format(date('c', mktime(0, 0, 0, 1, 1 + $days + $value, $year)));
  }

  return $output;
}

/**
 * @} End of "defgroup week_calendar".
 */

/**
 * @defgroup exam_calendar Exam calendar
 * @{
 * Oberstufen-Terminplan
 * @}
 */

/**
 * Form constructor for the exam calendar.
 *
 * @see pgn_pdfcalendar_select_exam_submit()
 * @see pgn_pdfcalendar_select_exam_validate()
 */
function pgn_pdfcalendar_select_exam($form, &$form_state) {

  $form['semester'] = _pgn_pdfcalendar_semester_select();

  $options = array(
    'q11' => 'Q11',
    'q12' => 'Q12',
  );

  $form['calendars'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Kalender',
    '#options' => $options,
    '#default_value' => array_keys($options),
  );

  $form['advanced_settings'] = pgn_pdfcalendar_advanced_settings();

  unset($form['advanced_settings']['settings']['calendars']);

  $textfield = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
  );

  $form['advanced_settings']['settings']['exam_date_start'] = array(
    '#title' => 'Beginn',
    '#description' => 'Format: TT.MM.JJJJ',
  ) + $textfield;

  $form['advanced_settings']['settings']['exam_date_end'] = array(
    '#title' => 'Ende',
    '#suffix' => '<div class="clearfix"></div>',
  ) + $textfield;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Druckversion aufrufen',
  );

  return $form;
}

/**
 * Form submission handler for pgn_pdfcalendar_select_exam().
 *
 * @see pgn_pdfcalendar_select_exam()
 */
function pgn_pdfcalendar_select_exam_submit($form, &$form_state) {
  $values = array();
  $values['mode'] = 'exam';

  list($semester, $year) = explode(':', $form_state['values']['semester']);

  $query = pgn_pdfcalendar_get_semester_period($semester, $year);

  $values['query']['semester'] = $semester;
  $values['query']['year'] = $year;
  $values['query']['start'] = $query['start'];
  $values['query']['end'] = $query['end'];

  if (!empty($form_state['values']['exam_date_start'])) {
    $values['query']['start'] = pgn_pdfcalendar_text_date($form_state['values']['exam_date_start']);
    unset($values['query']['semester']);
  }

  if (!empty($form_state['values']['exam_date_end'])) {
    $values['query']['end'] = pgn_pdfcalendar_text_date($form_state['values']['exam_date_end']);
  }
  pgn_pdfcalendar_submit($form_state, $values);
}

/**
 * Form validation handler for pgn_pdfcalendar_select_exam().
 *
 * @see pgn_pdfcalendar_select_exam()
 */
function pgn_pdfcalendar_select_exam_validate($form, &$form_state) {
  _pgn_pdfcalendar_validator($form_state['values']['exam_date_start'], 'exam_date_start');
  _pgn_pdfcalendar_validator($form_state['values']['exam_date_end'], 'exam_date_end');
}

/**
 * @} End of "defgroup exam_calendar".
 */

/**
 * Returns a form array for advanced settings.
 *
 * @return array
 *   Form array.
 */
function pgn_pdfcalendar_advanced_settings() {
  $form = array();

  $tokens = array('pgn', 'teacher', 'q11', 'q12', 'holiday');
  $calendar_ids = pgn_calendar_get_calendar_ids($tokens);
  $calendars = pgn_calendar_get_calendars($calendar_ids);

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Erweiterte Einstellungen',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $options = array();
  foreach ($calendars as $calendar) {
    if (!empty($calendar['title_custom'])) {
      $options[$calendar['token']] = $calendar['title_custom'];
    }
  }

  $default_value = array('pgn', 'q11', 'q12', 'holiday');

  $form['settings']['calendars'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Kalender',
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $form['settings']['color'] = array(
    '#type' => 'checkbox',
    '#title' => 'Optimiert für Schwarz-Weiß-Drucker',
  );

  return $form;
}

/**
 * Checks if the date string is in format DD.MM.YYYY.
 *
 * @param string $date
 *   A date string.
 * @param string $name
 *   Name of the form element.
 */
function _pgn_pdfcalendar_validator($date, $name) {
  if (!empty($date)) {
    if (!preg_match('#\d\d\.\d\d\.\d\d\d\d#', $date)) {
      form_set_error($name, t('Geben Sie das Datum im Format "TT.MM.JJJJ" an.'));
    }
  }
}

/**
 * Converts a date array to a date string.
 *
 * @param array $date
 *   A associative array keyed:
 *   - year
 *   - month
 *   - day
 *
 * @return string
 *   A date string: YYYY-MM-DD.
 */
function _pgn_calendar_date_to_str($date) {
  return $date['year'] . '-' . $date['month'] . '-' . $date['day'];
}

/**
 * Returns a form array to select semester.
 *
 * @return array
 *   Form array.
 */
function _pgn_pdfcalendar_semester_select() {
  $semesters = array(1, 2, 3);
  $year = pgn_pdfcalendar_get_semester_year();
  $options = array();

  foreach ($semesters as $semester) {
    $variables = array(
      'semester' => $semester,
      'year' => $year,
    );
    $options[$semester . ':' . $year] = theme('pgn_pdfcalendar_semester_title', $variables);
  }

  return array(
    '#type' => 'select',
    '#title' => 'Halbjahr',
    '#options' => $options,
    '#default_value' => pgn_pdfcalendar_get_semester_default(),
  );
}

/**
 * Retrieves the default options for the semester form select element.
 *
 * @param int $time
 *   Unix timestamp.
 *
 * @return array
 *   - 1:2013
 *   - 2:2013
 */
function pgn_pdfcalendar_get_semester_default($time = '') {
  if (empty($time)) {
    $time = time();
  }

  $month = date('n', $time);

  $year = pgn_pdfcalendar_get_semester_year($time);
  $output = '1:' . $year;

  if ($month > 1 && $month < 8) {
    $output = '2:' . $year;
  }

  return array($output);
}

/**
 * Returns the time period of a school semester.
 *
 * @param int $semester
 *   - 1: first semester (1. Aug - 1. March)
 *   - 2: second semester (1. Feb - 30. Sep)
 *   - 3: complete school year (1. Aug - 30. Sep)
 * @param int $year
 *   The beginning year of the school year, e. g. school year 2010/11
 *   -> year = 2010
 *
 * @return array
 *   An array with two keys:
 *   - start: start time in format '2012-01-13'
 *   - end: end time in format '2012-01-13'
 */
function pgn_pdfcalendar_get_semester_period($semester, $year) {
  $year1 = $year + 1;

  $output = array();
  $output['semester'] = $semester;
  switch ($semester) {

    case 1:
      $output['start'] = $year . '-08-01';
      $output['end'] = $year1 . '-03-01';
      break;

    case 2:
      $output['start'] = $year1 . '-02-17';
      $output['end'] = $year1 . '-09-15';
      break;

    case 3:
      $output['start'] = $year . '-08-01';
      $output['end'] = $year1 . '-08-30';
      break;
  }

  return $output;
}

/**
 * Retrieves to a given timestamp the corresponding first school year.
 *
 * Example: School year 2011/2012
 *
 * Jul 2011 -> return 2010
 * Aug 2011 -> return 2011
 * Sep 2011 -> return 2011
 * Oct 2011 -> return 2011
 * Nov 2011 -> return 2011
 * Dec 2011 -> return 2011
 * Jan 2012 -> return 2011
 * Feb 2012 -> return 2011
 * Mar 2012 -> return 2011
 * Apr 2012 -> return 2011
 * Jun 2012 -> return 2011
 * Jul 2012 -> return 2011
 * Aug 2012 -> return 2012
 * Sep 2012 -> return 2012
 * Oct 2012 -> return 2012
 *
 * @param int $time
 *   Unix timestamp.
 *
 * @return int
 *   Year.
 */
function pgn_pdfcalendar_get_semester_year($time = '') {
  if (empty($time)) {
    $time = time();
  }
  $month = date('n', $time);
  $year = date('Y', $time);
  if ($month < 8) {
    $year--;
  }
  return $year;
}

/**
 * Initializes the TCPDF library.
 */
function _pgn_pdfcalendar_tcpdf_init() {
  $path = 'sites/all/libraries/tcpdf/tcpdf.php';
  require_once DRUPAL_ROOT . '/' . $path;

  // Disable caching for generated PDFs, as Drupal doesn't ouput the proper
  // headers from the cache.
  $GLOBALS['conf']['cache'] = FALSE;
  module_load_include('inc', 'pgn_pdfcalendar', 'pgn_pdfcalendar.class');
}

/**
 * Sorts a calendar events array.
 *
 * Sort order:
 * - Year
 * - Month
 * - Week number
 * - Week nummer : Day of month with leading zero
 * - Hour : Minute (Leading zeros.)
 *
 * Input:
 *
 * @code
 * Array
 * (
 *     [pirckheimer.gymnasium@googlemail.com] => Array
 *         (
 *             [0] => Array
 *                 (
 *                     [title] => Besondere Prüfung Deutsch
 *                     ...
 *                 )
 *
 *             [1] => Array
 *                 (
 *                     [title] => Besondere Prüfung Mathematik
 *                     ...
 * @endcode
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [2013] => Array
 *         (
 *             [9] => Array
 *                 (
 *                     [36] => Array
 *                         (
 *                             [36:04] => Array
 *                                 (
 *                                     [00:00] => Array
 *                                         (
 *                                             [0] => Array
 *                                                 (
 *                                                     [title] => Besonder ..
 *                                                     [content] =>
 * @endcode
 *
 * @param array $calendar_events
 *   Calendar events array.
 *
 * @return array
 *   The sorted calendar events array.
 */
function _pgn_pdfcalendar_ksort($calendar_events) {
  $output = array();
  foreach ($calendar_events as $events) {
    foreach ($events as $event) {
      if (!isset($event['shift'])) {
        $start = $event['start'];
      }
      else {
        $start = $event['shift'];
      }

      $year = $start['year'];
      $month = $start['month'];
      $week = $start['week'];
      $day = $start['week'] . ':' . $start['day_zero'];
      $time = $start['hour'] . ':' . $start['minute'];

      $output[$year][$month][$week][$day][$time][] = $event;
    }
  }

  return $output;
}

/**
 * Splits a event which lasts more days in events for each day.
 *
 * @param array $calendar_events
 *   Calendar events.
 *
 * @return array
 *   Splitted calendar events array.
 */
function pgn_pdfcalendar_event_splitter($calendar_events) {
  foreach ($calendar_events as $calendar_id => $events) {
    foreach ($events as $event) {

      if ($event['more_days'] == TRUE) {
        $start = $event['start']['time_stamp'];
        $end = $event['end']['time_stamp'];
        $shift = $start;
        $shift_count = 0;

        $day = 24 * 60 * 60;

        while ($shift < $end - $day) {
          $shift = $shift + $day;
          $shift_count++;

          $event['shift'] = pgn_calendar_date_format($shift);
          $event['shift_count'] = $shift_count;

          $calendar_events[$calendar_id][] = $event;
        }
      }
    }
  }
  return $calendar_events;
}

/**
 * Converts an array to a base64 encoded path string.
 *
 * Input:
 *
 * @code
 * Array
 * (
 *     [mode] => main
 *     [query] => Array
 *         (
 *             [semester] => 1
 *             [year] => 2013
 *             [start] => 2013-08-01
 *             [end] => 2014-03-01
 *         )
 *
 *     [format] => Array
 *         (
 *             [new_column] => 0
 *             [display_description] => 0
 *             [color] => 1
 *         )
 *
 *     [calendars] => Array
 *         (
 *             [pgn] => pgn
 *             [q11] => q11
 *             [q12] => q12
 *         )
 *
 * )
 * @endcode
 *
 * Output:
 *
 * @code
 * eCVEQW0lOEUlQzElMEElODMwJTEwRCVGRmUlRUZCYiVEQWIlRDclOEYlOTElQURuSyU0MCUxM00lM
 * jJF JThBJUZGJURFRCU4MyVGNSVEMEt4ZGZ2JTg2JUYwJTgyJTFGJTFGJTFGJTE4bCVDN1AlRUZIJ
 * URBJTI0 JUJDJTIyTDMlQkIlMDVqJURBbSUxNSU4MiVFNyU4MSU3RCU2MCU5N3QlODklMjBzZGFyJ
 * TE5SyUyMVVO JUZCJTQwLmxOJUIxJUZGJTE3JUEyJTJBJUM0JTk2USUwOGwlQkElOTN2JTI5JTg0J
 * URBJUI0JUQ1JUUz JTBEJUUxaSVERCU0MCUyMTUlQUIlRDQlOUNMJTg2JURGTWslRkJ5JTg4JUUzN
 * CU4QSUxNCVCRCUyM3Ql REElOEYlM0QtTSVDNyVCRXV6JTBDJURBJTFFciU1QyUxMCVGRDYlMEUlN
 * 0IlQTBMdyVBMyVCRCVBNSUz RSVGNiU5MiVGMyVDNyVFOSVCOGQlN0MlOTklQkMlRTlHJTkzJTk0J
 * TdGJUE4JTNDJUQzJUJBfiUwMSVB QyU2MCU1Q0Q=
 * @endcode
 *
 * @param array $array
 *   An array.
 *
 * @return string
 *   Base64 encoded string.
 *
 * @see _pgn_calendar_path_decode()
 */
function _pgn_calendar_path_encode($array) {
  return base64_encode(rawurlencode(gzcompress(serialize($array), 9)));
}

/**
 * Converts an based64 encoded path string to a array.
 *
 * Input:
 *
 * @code
 * eCVEQW0lOEUlQzElMEElODMwJTEwRCVGRmUlRUZCYiVEQWIlRDclOEYlOTElQURuSyU0MCUxM00lM
 * jJF JThBJUZGJURFRCU4MyVGNSVEMEt4ZGZ2JTg2JUYwJTgyJTFGJTFGJTFGJTE4bCVDN1AlRUZIJ
 * URBJTI0 JUJDJTIyTDMlQkIlMDVqJURBbSUxNSU4MiVFNyU4MSU3RCU2MCU5N3QlODklMjBzZGFyJ
 * TE5SyUyMVVO JUZCJTQwLmxOJUIxJUZGJTE3JUEyJTJBJUM0JTk2USUwOGwlQkElOTN2JTI5JTg0J
 * URBJUI0JUQ1JUUz JTBEJUUxaSVERCU0MCUyMTUlQUIlRDQlOUNMJTg2JURGTWslRkJ5JTg4JUUzN
 * CU4QSUxNCVCRCUyM3Ql REElOEYlM0QtTSVDNyVCRXV6JTBDJURBJTFFciU1QyUxMCVGRDYlMEUlN
 * 0IlQTBMdyVBMyVCRCVBNSUz RSVGNiU5MiVGMyVDNyVFOSVCOGQlN0MlOTklQkMlRTlHJTkzJTk0J
 * TdGJUE4JTNDJUQzJUJBfiUwMSVB QyU2MCU1Q0Q=
 * @endcode
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [mode] => main
 *     [query] => Array
 *         (
 *             [semester] => 1
 *             [year] => 2013
 *             [start] => 2013-08-01
 *             [end] => 2014-03-01
 *         )
 *
 *     [format] => Array
 *         (
 *             [new_column] => 0
 *             [display_description] => 0
 *             [color] => 1
 *         )
 *
 *     [calendars] => Array
 *         (
 *             [pgn] => pgn
 *             [q11] => q11
 *             [q12] => q12
 *         )
 *
 * )
 * @endcode
 *
 * @param string $path
 *   A base64 encoded path string.
 *
 * @return array
 *   An array.
 *
 * @see _pgn_calendar_path_encode()
 */
function _pgn_calendar_path_decode($path) {
  return unserialize(gzuncompress(rawurldecode(base64_decode($path))));
}

/**
 * Loads css files and returns its as a string wrapped in style tags.
 *
 * @param string $file_name
 *   Short filename 'pgn_pdfcalendar_week.css' -> 'week'.
 *
 * @return string
 *   Embeded css in style tags.
 */
function pgn_pdfcalendar_css_load($file_name) {
  $path = drupal_get_path('module', 'pgn_pdfcalendar');
  $path = DRUPAL_ROOT . '/' . $path . '/css/';

  $files = array(
    'pgn_pdfcalendar_all.css',
    'pgn_pdfcalendar_' . $file_name . '.css',
  );
  $style = '';
  foreach ($files as $file) {
    $style .= file_get_contents($path . $file);
  }
  return '<style>' . $style . '</style>';
}
