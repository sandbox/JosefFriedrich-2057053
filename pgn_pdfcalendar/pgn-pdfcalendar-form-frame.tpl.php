<?php
/**
 * @file
 * Displays a generation form for a calendar.
 *
 * Available variables:
 *   - $image: A preview image of the calendar.
 *   - $form: The form of the calendar.
 *
 * @ingroup themeable
 */
?>
<div class="pgn-pdfcalendar-form-frame">

  <div class="pgn-image">
  <?php print render($image); ?>
  </div>

  <div class="pgn-form">
  <?php print render($form); ?>
  </div>

</div>
