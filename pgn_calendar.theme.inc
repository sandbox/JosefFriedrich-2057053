<?php

/**
 * @file
 * Theme functions.
 */

/**
 * @defgroup theme_functions Theme functions
 * @{
 * The theme functions implemented by the module.
 */

/**
 * Returns a list of calendars.
 *
 * @param array $variables
 *   An associative array containing:
 *   - events
 */
function theme_pgn_calendar_list($variables) {
  $events = $variables['events'];
  $output = '';
  foreach ($events as $event) {
    $date = theme('pgn_calendar_date', array('event' => $event));
    $output .= '<p>' . $date . '</p>';
    $output .= '<small>' . $event['title'] . '</small>';
  }
  return '<div id="pgn-calendar">' . $output . '</div>';
}

/**
 * Returns a formated date.
 *
 * @param array $variables
 *   An associative array containing:
 *   - event
 *   - format
 *
 * @return string
 *   Date format like: Tue 13. September 2011 7:55 - 9:25
 */
function theme_pgn_calendar_date($variables) {
  $event = $variables['event'];
  $format = (array) $variables['format'];

  $format += array(
    'short' => FALSE,
    'only_end' => FALSE,
  );

  $start = $event['start'];
  $end = $event['end'];
  $end_1 = $event['end_minus_1day'];

  if ($format['short']) {
    $start['date'] = $start['date_short'];
    $end['date'] = $end['date_short'];
    $end_1['date'] = $end_1['date_short'];
  }

  $output = array();

  // Start.
  $output['start'] = '';
  if (!$format['only_end']) {
    $output['start'] .= $start['date'];
  }
  if ($start['has_time']) {
    $output['start'] .= ' ' . $start['time'];
  }

  // End.
  if (!empty($end)) {
    $output['end'] = '';
    if ($event['one_day'] == FALSE) {
      if ($event['all_day'] == TRUE) {
        $output['end'] .= ' ' . $end_1['date'];
      }
      elseif ($end['date'] != $start['date']) {
        $output['end'] .= ' ' . $end['date'];
      }

      if ($end['has_time'] && $start['date_time'] != $end['date_time']) {
        $output['end'] .= ' ' . $end['time'];
      }
    }
  }

  // Combine start + end.
  if (!empty($output['end'])) {
    $output = implode(' – ', array($output['start'], $output['end']));
  }
  else {
    $output = $output['start'];
  }

  return trim($output);
}

/**
 * @} End of "defgroup theme_functions".
 */
