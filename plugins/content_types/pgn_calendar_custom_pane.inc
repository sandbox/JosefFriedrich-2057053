<?php

/**
 * @file
 * Panel pane of the PGN calendar.
 */

$plugin = array(
  'single' => TRUE,
  'title' => 'Kalender',
  'description' => 'Der PGN-Kalender',
  'category' => 'PGN',
  'render callback' => 'pgn_calendar_custom_pane_render',
  'admin info' => 'pgn_calendar_custom_pane_admin_info',
);

/**
 * Form submit function of pgn_calendar_custom_pane_edit_form().
 */
function pgn_calendar_custom_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Callback for 'render callback' panel pane callback.
 */
function pgn_calendar_custom_pane_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = 'Termine';
  $pgn_calendar_block = module_invoke('pgn_calendar', 'block_view', 'pgn_calendar');
  $block->content = $pgn_calendar_block['content'];
  return $block;
}

/**
 * Callback for 'admin info' panel pane callback.
 */
function pgn_calendar_custom_pane_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = 'Termine';
    $block->content = 'Die 10 anstehenden Termine';
    return $block;
  }
}
